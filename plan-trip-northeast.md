You can find my interpretation of what Bo wrote
[here](https://goo.gl/maps/xhivoGBfcVWKwnsBA). Let me know if that's incorrect.

I just want to relax while traveling—my objective is maximum grooviness, man—so
I don't care too much about meandering about somewhat and not taking the most
direct routes from one sleeping place to the next. Certainly many miles can be
cut out of the below. And we don't *really* need to visit as many fancy schools
as possible; that's just what I'm interesting in doing. Meanwhile, I don't
particularly care whether we go to Cape Cod.

You may look at this and think "he's crazy! we can't go to all of those places
while also doing all of that driving; there's simply no time," and you may be
right, in which case we shall have to excise some things. But it is nice how
relatively close together everything is up there, unlike down here. I figure
we'd spend no more than an hour or two walking around each university, but even
that may be too much.

With those prefatory remarks, I present my list of places I would like to visit,
in order, given the constraints of staying with Bo's relatives in particular
places. You can find some rough maps with this route, broken up into subroutes
because Google Maps has a silly limit (of 10??) on number of waypoints,
[here](https://goo.gl/maps/XkJUS5qUSYXvSEjv8) including nights 1–3 in Jamestown
and Canton; [here](https://goo.gl/maps/Cxd8ksCRxkvN4n5F8) including nights 4–7
in Fayette, Boston, and Cape Cod; and
[here](https://goo.gl/maps/M8CH1fdZMwqnVssdA) including nights 8 and 9 on Long
Island (though I've omitted "Long Island" as an explicit stop).

1. Carnegie Mellon
2. Cathedral of Learning
3. Superstar Car Wash, or the [building](https://archive.ph/MT6Aw) that once was
   S.C.W., anyway
4. Cornell
5. Vale Cemetery
6. Dartmouth
7.  MIT
8. (Morse Elementary—significant only because it's between the two universities
   and so we'd walk by it anyway)
9. Harvard
10. Brown
11. Yale
12. Columbia
13. Central Park
14. 243 E. Clinton Ave., Hempstead NY 11575
15. 40°41'24.2"N 73°34'32.0"W
16. Princeton
17. Penn

We wouldn't drive precisely the route indicated in the links above, but as a
first approximation, this would supposedly be about
```
15:53 + 13:30 + 10:44 = 38:127 = 40:07
```
(forty hours and seven minutes) of driving time spread across nine days, which
doesn't sound too bad to me, though of course some days would have much more
driving than others.

Oh, and we've got to go to Fire Island. Because Tanya Donelly.

My questions at this point are as follows.
- Where on Long Island do we want to stay? It is, after all, quite long. Google
  Maps just puts a marker in the middle as you might expect, but I'm guessing
  we'd prefer to sleep closer to the city for at least one night for easy
  access.
  + I guess I can answer this myself based on Bo's Airbnb link.
- Are we trying to maximize the number of states we can say we've visited?
  'Cause it wouldn't be *that* hard to swing down to New Jersey, Delaware, and
  Maryland. I've never been to any of those, as far as I know, but maybe the
  rest of you have.
- While in the vicinity of New York, will we travel by train and leave the
  rental car parked where we sleep?
- While driving, do we want to avoid tolls? Do we want to avoid interstate
  highways sometimes if some smaller highway is more scenic? Or are we always
  going from one place to another as fast as possible?
- What's the compelling reason to stay in Canton? I assume one exists. But if we
  actually went to Schenectady and Hanover as I desire, then sleeping in
  Schenectady or Albany or thereabouts would make more sense.

Now, where do I suggest that we sleep on this route? Taking it as a challenge to
construct the cheapest set of lodgingplaces possible, I have come up with the
following. The two Airbnb links may get booked by someone else before you read
this.

1. [A tiny house near Canton](https://www.airbnb.com/rooms/44210341?adults=4&check_in=2021-05-16&check_out=2021-05-17&federated_search_id=d932b5ab-f67c-4ce4-8d8f-ade0f003b240&source_impression_id=p3_1619916008_bcwieiqkiBeM%2B9Eo) for $88
2. Spencer said we might be able to stay with him at least one night if we're
   all OK with a bit of crowding and aren't too set on sleeping nearer to Cape
   Cod for two nights.
3. [Some place near the edge of Queens](https://www.airbnb.com/rooms/44261666?adults=4&check_in=2021-05-21&check_out=2021-05-23&federated_search_id=c211813f-65d2-42f2-a513-63d39dcb5b67&source_impression_id=p3_1619923337_ljd705Ptj%2BmQ86Hy&guests=1)
   for $237…but maybe the point of specifying Long Island was to be somewhat
   farther from the city. We should just have another call and work this out.
   Perhaps someone else should make the decision for me.
